const express = require("express");
const cors = require("cors");

require("dotenv").config();
const app = express();

const db = require("./app/models");
const { initial } = require("./data");
// const { route } = require("./app/routes/auth.route");

app.use(cors());
app.use(express.json())

db.mongoose
    .connect(`mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`)
    .then(() => {
        console.log("Connect mongoDB Successfully");
        initial();
    })
    .catch((err) => {
        console.error("Connection error", err);
        process.exit(); 
    })
app.get("/", (req, res) => {
    res.json({
        message: "Welcome to JWT"
    })
})

app.use("/api/auth", require("./app/routes/auth.route"))

const PORT = 8000 || process.env.ENV_PORT

app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`)
})

