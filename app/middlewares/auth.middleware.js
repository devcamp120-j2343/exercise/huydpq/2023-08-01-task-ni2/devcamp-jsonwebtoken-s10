const db = require("../models");

const User = db.user;
const ROLE = db.ROLE;

const checkDuplicateUserName = async (req, res, next) => {
    try {
        const exitUserName = await User.findOne({
            username: req.body.username
        })

        if(exitUserName) {
            res.status(400).json({
                message: "Username is already in use"
            })
            return;
        }

        next();
    } catch (error) {
        console.error("Interal Server Error", error);
        process.exit(); 
    }
}

module.exports = {checkDuplicateUserName}