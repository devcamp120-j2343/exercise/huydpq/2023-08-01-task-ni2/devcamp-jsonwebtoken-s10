const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        reuired: true
    },
    password: {
        type: String,
        unique: true,
        reuired: true
    },
    roles: [
        {
        type: mongoose.Types.ObjectId,
        ref: "role"
        }
    ] 
    
}, {
    timestamps: true
})

module.exports = mongoose.model("user", userSchema)