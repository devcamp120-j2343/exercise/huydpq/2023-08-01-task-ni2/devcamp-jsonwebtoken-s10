const express = require("express");
const { checkDuplicateUserName } = require("../middlewares/auth.middleware");
const { signup, login } = require("../controllers/auth.controller");
const route = express.Router();
// const db = require("./app/models");

route.post("/signup", checkDuplicateUserName, signup, (req, res) => {
    res.send("Signup Api")
})

route.post("/login",login , (req, res) => {
    res.send("login Api")
})

module.exports = route;