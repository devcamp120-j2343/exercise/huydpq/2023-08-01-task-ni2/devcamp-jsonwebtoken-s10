const db = require("../models");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken")

const signup = async (req, res) => {
    try {
        const userRole = await db.role.findOne({
            name: "user"
        })
    
        const user = new db.user({
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password, 8),
            roles: userRole
        })
        
        await user.save()
        res.status(200).json({
            message: "Create user successfully"
        })
    } catch (error) {
        res.status(500).json({
            message: "Interal Server Error", 
            err: message.error
        })
    }
}

const login = async (req, res) => {
    try {
        const exitUser = await db.user.findOne({
            username: req.body.username
        }).populate("roles");

        if(!exitUser){
            return res.status(400).json({
                message: "Not Found"
            })
        }
        // compareSync: so sans
        let passwordIsValid = bcrypt.compareSync(
            req.body.password,
            exitUser.password
        )
        if(!passwordIsValid){
            return res.status(400).json({
                message: "Invalid password"
            })
        }

        const sercetKey = process.env.JWT_SECRET;

        const token =  jwt.sign({id: exitUser._id}, sercetKey, {
            algorithm: "HS256", // Dạng mã hóa
            allowInsecureKeySizes: true,
            expiresIn: 86500 // thời gian tồn tại 
        })
        res.status(200).json({
            accessToken: token, 
            
        })
        
    } catch (error) {
        res.status(500).json({
            message: "Interal Server Error", 
            err: message.error
        })
    }
}

module.exports = {signup, login}