const db = require("./app/models");

const initial = async (req, res) => {
    try {
        const roleModel = db.role;
         const count = await roleModel.estimatedDocumentCount();

         if(count === 0){
            await new roleModel({
                name: db.ROLE[0]
            }).save()
            await new roleModel({
                name: db.ROLE[1]
            }).save()
            await new roleModel({
                name: db.ROLE[2]
            }).save()
         }
    } catch (error) {
        console.error("init data err", error);
        process.exit()
    
    }
}
module.exports = {initial}